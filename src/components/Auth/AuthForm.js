import React from 'react'
import {Keyboard} from 'react-native'
import {InputText} from '../UI/InputText'
import {FormContainer} from '../UI/FormContainer'
import {Button} from '../UI/Button'
import Toast from 'react-native-simple-toast'
import {NavigationEvents} from 'react-navigation'

const REGEX = {
  mail: /\S+@\S+/,
  phone: /\d{8,10}/
}

export class AuthForm extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      firstValue: '',
      password: '',
      confirmPassword: '',
      validField: false,
      validPassword: false,
      isBlur: false
    }

    this.firstValue = React.createRef()
    this.password = React.createRef()
    this.confirmPassword = React.createRef()
  }

  render() {
    const {isSignup, mainButtonText} = this.props
    const buttonEnabled = this.isButtonEnabled()
    return (
      <FormContainer center justify>
        <NavigationEvents
          onWillFocus={payload => this.setState({isBlur: false})}
          onWillBlur={payload => this.setState({isBlur: true})}
        />

        <InputText
          forwardedRef={this.firstValue}
          label='Email or Phone'
          onChangeText={value => this.setState({firstValue: value})}
          value={this.state.firstValue}
          autoCapitalize='none'
          returnKeyType='next'
          onEndEditing={() => {
            if (
              this.state.firstValue &&
              this.password &&
              this.password.current
            ) {
              this.validateField('first')
              this.password.current.focus()
            }
          }}
        />
        <InputText
          forwardedRef={this.password}
          value={this.state.password}
          label='Password'
          secureTextEntry
          onChangeText={password => this.setState({password})}
          onEndEditing={() => {
            if (
              this.state.password &&
              (!isSignup || this.state.confirmPassword)
            ) {
              this.validateField('password')
            }
          }}
        />
        {isSignup && (
          <InputText
            forwardedRef={this.confirmPassword}
            value={this.state.confirmPassword}
            label='Repeat Password'
            secureTextEntry
            onChangeText={confirmPassword => this.setState({confirmPassword})}
            onEndEditing={() => {
              if (this.state.password && this.state.confirmPassword) {
                this.validateField('password')
              }
            }}
          />
        )}
        <Button
          disabled={!buttonEnabled}
          white
          onPress={this.onSubmit}
          isLoading={this.props.isLoading}
        >
          {mainButtonText}
        </Button>
      </FormContainer>
    )
  }

  validateField = type => {
    const {firstValue, password, confirmPassword, isBlur} = this.state
    if (!isBlur) {
      switch (type) {
        case 'first': {
          const isValidPhone = REGEX['phone'].test(firstValue)
          const isValidEmail = REGEX['mail'].test(firstValue)
          if (!isValidEmail && !isValidPhone) {
            this.setState({validField: false})
            return Toast.show(
              'please input a valid email address or phone number.'
            )
          }
          return this.setState({validField: true})
        }
        case 'password': {
          if (this.props.isSignup) {
            if (password !== confirmPassword) {
              this.setState({validPassword: false})
              return Toast.show('The passwords should match.')
            }
          }
          return this.setState({validPassword: true})
        }
      }
    }
  }

  onSubmit = () => {
    const {firstValue, password} = this.state
    Keyboard.dismiss()
    return this.props.onSubmit({firstValue, password})
  }

  isButtonEnabled = () => {
    const {validField, validPassword} = this.state
    return validField && validPassword
  }
}
