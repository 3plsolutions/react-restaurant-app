import React from 'react'
import styled from 'styled-components'
import {Alert, TouchableWithoutFeedback} from 'react-native'
import {InputText} from '../UI/InputText'
import {Button} from '../UI/Button'
import NavigationService from 'src/utils/NavigationService'

const REGEX = {
  mail: /\S+@\S+/,
  phone: /\d{8,10}/
}

export class RememberPassword extends React.PureComponent {
  state = {
    value: '',
    valueIsValid: false
  }
  render() {
    return (
      <Container>
        <Title>
          Remember password
        </Title>
        <InputText
          label='email or Phone'
          onChangeText={value => this.setState({value})}
          value={this.state.value}
          autoCapitalize='none'
          onEndEditing={() => this.validateField()}
        />
        <Button
          white
          onPress={this.props.onSubmit}
          disabled={!this.state.valueIsValid}>
            Send Link
        </Button>
        <TouchableWithoutFeedback onPress={() => NavigationService.goBack()}>
          <RememberText>
            Remember it?<Login> Log In</Login>
          </RememberText>
        </TouchableWithoutFeedback>
      </Container>
    )
  }

  validateField = () => {
    const {value} = this.state
    const isValidPhone = REGEX['phone'].test(value)
    const isValidEmail = REGEX['mail'].test(value)
    if (!isValidEmail && !isValidPhone) {
      return Alert.alert('Error', 'please input a valid email address or phone number')
    }
    return this.setState({valueIsValid: true})
  }
}

const Container = styled.View`
  flex:1;
  justify-content: center;
  alignItems:center;
`
const RememberText = styled.Text`
  font-size: 14;
  color: white;
`
const Login = styled.Text`
  font-size: 14;
  color: white;
  font-weight: bold;
`

const Title = styled.Text`
  marginBottom: 50;
  font-size: 22;
  color: white;
  font-weight: bold;
`
