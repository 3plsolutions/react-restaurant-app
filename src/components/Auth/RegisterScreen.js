import React from 'react'
import {Image, TouchableWithoutFeedback} from 'react-native'
import styled from 'styled-components'
import PropTypes from 'prop-types'

import {AuthForm} from './AuthForm'
import NavigationService from 'src/utils/NavigationService'

export class RegisterScreen extends React.PureComponent {
  static propTypes = {
    footNote: PropTypes.shape({
      text: PropTypes.string,
      actionText: PropTypes.string,
      actionRef: PropTypes.string
    }),
    onSubmit: PropTypes.func
  }

  static defaultProps = {
    onSubmit: () => {}
  }

  render() {
    const {mainButtonText} = this.props
    return (
      <Container>
        <Header>
          <TouchableWithoutFeedback onPress={() => NavigationService.goBack()}>
            <Image source={require('src/assets/icons/x-close.png')} />
          </TouchableWithoutFeedback>
        </Header>
        <ContentContainer>
          <AuthForm
            onSubmit={this.props.onSubmit}
            isSignup
            mainButtonText={mainButtonText}
          />
        </ContentContainer>
      </Container>
    )
  }
}

const Container = styled.View`
  flex:1;
  justifyContent: center;
  alignItems: center;
  flexDirection: column;
`

const ContentContainer = styled.View`
  flex: 1;
  width: 100%;  
  justifyContent: flex-start;  
  alignItems: center;
  flexDirection: column; 
  marginTop: 50%;
  minHeight: 350;
`
const Header = styled.View`      
   position: absolute;  
   top: 30;
   left: 20;
   padding: 10px;    
`
