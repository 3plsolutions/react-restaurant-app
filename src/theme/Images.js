import LeonTanImage from 'src/assets/images/leon-tan-unsplash.jpg'
import CarsImage from 'src/assets/images/ashutosh-jangid-1214227-unsplash.jpg'
import LocationMark from 'src/assets/images/location-mark.png'
const images = {
  LeonTanImage,
  CarsImage,
  LocationMark
}

export default images
