import React from 'react'
import styled from 'styled-components'

import StyledText from 'src/components/StyledText'

export default function BottomDrawer () {
  return (
    <>
      <TitleContainer>
        <StyledText style={{fontWeight: '500', fontSize: 18}}>Near me...</StyledText>
      </TitleContainer>
    </>
  )
}

const TitleContainer = styled.TouchableOpacity`
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
  font-size: 18px;
  padding: 15px 30px;
  color: #000;
`
