import React from 'react'
import {
  Alert,
  ImageBackground,
  Modal,
  View,
  ActivityIndicator,
  StyleSheet
} from 'react-native'
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'

import api from 'src/api'
import {RegisterScreen} from 'src/components/Auth/RegisterScreen'
import {extractErrorMessage, log} from 'src/utils/fn'

const modalStyle = StyleSheet.create({
  modalBackground: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'
  },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF',
    height: 100,
    width: 100,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
  }
})

export class UserRegister extends React.PureComponent {
  state = {
    isLoading: false,
    requireCode: false
  }

  render() {
    return (
      <KeyboardAwareScrollView
        contentContainerStyle={{flexGrow: 1}}
        keyboardShouldPersistTaps='always'
      >
        <ImageBackground
          resizeMode='cover'
          source={require('src/assets/images/log-in-bg.png')}
          style={{height: '100%', width: '100%'}}
        >
          <RegisterScreen
            mainButtonText='Sign up'
            footNote={{
              text: 'Have an account already?',
              actionText: 'Sign in',
              actionRef: 'SignIn'
            }}
            onSubmit={this.handleFormOnSubmit}
            requireCode={this.state.requireCode}
            isLoading={this.state.isLoading}
          />
        </ImageBackground>
        <Modal
          transparent
          animationType={'none'}
          visible={this.state.isLoading}
          onRequestClose={() => {
            console.log('close modal')
          }}
        >
          <View style={modalStyle.modalBackground}>
            <View style={modalStyle.activityIndicatorWrapper}>
              <ActivityIndicator animating size='large' />
            </View>
          </View>
        </Modal>
      </KeyboardAwareScrollView>
    )
  }

  handleFormOnSubmit = async ({firstValue, password}, formType) => {
    const {navigation} = this.props
    // this code is likely to disappear
    // if (formType === AuthForm.FORM_TYPE.SMS) {
    //   if (isCode) {
    //     // todo: send received code to server
    //     return
    //   }

    //   this.setState({requireCode: true})
    //   // todo: send phone number to server
    //   return
    // }

    if (!firstValue || !password) {
      return
    }

    try {
      this.setState({isLoading: true})
      const response = await api.create({
        route: `register`,
        data: {
          email: firstValue.trim(),
          password
        }
      })

      this.setState({isLoading: false})

      if (!response.errors) {
        Alert.alert(
          'Your account has been created successfully. You can now sign in!'
        )
        navigation.navigate('SignIn')
      } else {
        // TODO: treat error
        _alertError(response)
      }
    } catch (e) {
      _alertError(e)
      this.setState({isLoading: false})
    }
  }
}

// //
function _alertError(errorObject = {}) {
  Alert.alert('Whoops!', extractErrorMessage(errorObject))
  log.error('SERVER ERROR', errorObject)
}
